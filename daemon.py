#!/usr/bin/python3
"""Example daemon to listen to GitLab webhooks
"""
import http.server
import json
import socketserver
import logging

socketserver.TCPServer.allow_reuse_address = True

logger = logging.getLogger()
logging.basicConfig(level=logging.DEBUG)

class WebhookHandler(http.server.SimpleHTTPRequestHandler):
    def log_message(self, format, *args):
        pass
    def do_POST(self):
        length = int(self.headers.get('content-length', 0))
        event = self.headers.get('x-gitlab-event')
        token = self.headers.get('x-gitlab-token')
        data = json.loads(self.rfile.read(length).decode())
        if event == 'Job Hook':
            logger.info('event for (%s for %d: %s)',
                        event,
                        data['build_id'],
                        data['build_status'])
        else:
            logger.info('event (%s)', event)
        self.send_response(200)
        self.end_headers()

w = socketserver.TCPServer(("", 9875), WebhookHandler)
try:
    w.serve_forever()
except:
    w.shutdown()